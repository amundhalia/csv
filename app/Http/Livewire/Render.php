<?php

namespace App\Http\Livewire;

use Carbon\Carbon;
use Livewire\Component;
use League\Csv\Reader;
use League\Csv\Statement;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;

/**
 * This is livewire class this reads CSV and renders it on view
 * @package livewire
 * @subpackage league
 *
*/
class Render extends Component
{
    /**
     * This variable contains data used on the view table
     * @param public
    */
    public $records;

    /**
     * This is the event listener that evokes a function on the event broadcast
     * @param protected
    */
    protected $listeners = ['dataLoaded' => 'getFile'];

    /**
     * Array provided in this Test
     * @param protected
    */
    protected $validChars = ['2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C',
                             'D', 'E', 'F', 'G', 'H', 'J', 'K','L', 'M', 'N', 'P',
                             'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];

    /**
     * Gets the file from storage and converts it in collection
     * @param null
     * @return null
     * @access public
    */
    public function getFile()
    {
        //create empty array
        $data = array();
        //retrieve csv file from storage
        $file = storage_path('app/transactions/'.Cache::get('file'));
        //check if file exists
        if (Storage::exists('transactions/'.Cache::get('file'))) {
            //read csv
            $csv = Reader::createFromPath($file, 'r');
            $csv->setHeaderOffset(0);
            $stmt = Statement::create();
            $records = $stmt->process($csv);
            //run for each csv record
            foreach ($records as $record) {
                $record['status'] = $this->verifyKey($record['TransactionNumber']); //check if transaction number is valid
                $record['Date'] = Carbon::parse($record['Date'])->timestamp; //convert date to timestamp for easy sorting
                $data[] = $record;
            }
            // convert data array to collection and sort by Date
            $this->records = collect($data)->sortBy('Date');
        }
    }

    /**
     * Renders data from this page to view
     * @return view
     * @access public
    */
    public function render()
    {
        //load uploded file after page refresh
        if (Cache::has('file')) {
            $this->getFile();
        }

        return view('livewire.render'); //return view
    }

    private function verifyKey($key)
    {
        //check lenght of string
        if (Str::length($key) != 10){
            return false;
        }
        $checkDigit = $this->generateCheckCharacter(Str::substr(Str::upper($key), 0, 9));
        return Str::substr($key, 9) == $checkDigit;
    }

    private function generateCheckCharacter($input)
    {
        $factor = 2;
        $sum = 0;
        $n = Count($this->validChars);

        for ($i=Str::length($input)-1; $i >= 0; $i--) {
            $codePoint = array_search($input[$i], $this->validChars);
            $addend = $factor * $codePoint;
            // Alternate the "factor" that each "codePoint" is multiplied by
            $factor = ($factor == 2) ? 1 : 2;
            // Sum the digits of the "addend" as expressed in base "n"
            $addend = ($addend / $n) + ($addend % $n);
            $sum += $addend;
        }

        // Calculate the number that must be added to the "sum"
        // to make it divisible by "n"
        $remainder = $sum % $n;
        $checkCodePoint = ($n - $remainder) % $n;
        return $this->validChars[$checkCodePoint];
    }
}
