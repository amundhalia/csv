<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Livewire\WithFileUploads;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Storage;
use File;

/**
 * This class is is for uploding csv
 * @package livewire
*/
class Upload extends Component
{
    use WithFileUploads;

    /**
     * Global variable for this livewire coponent
     * @param file
    */
    public $transaction;

    /**
     * This function saves uploded file into storage Directory
     * @access public
     * @return null
    */
    public function save()
    {
        // validate file
        $this->validate([
            'transaction' => 'required|mimes:csv,txt|max:1024', // Field is required // only accept CSV //1MB Max
        ]);
        // if directory do not exist create it
        if(!File::isDirectory(storage_path('app/transactions'))){
            Storage::makeDirectory('app/transactions');
        }
        // save uploded file to storage/app/transactions
        $this->transaction->storeAs('transactions', $this->transaction->getClientOriginalName());
        // add file name to Cache server in this case Redis
        Cache::put('file', $this->transaction->getClientOriginalName());
        // Broadcast global event
        $this->emit('dataLoaded');

    }

    /**
     * Renders view located at resources/views/livewire/...
     * @access public
     * @return view
    */
    public function render()
    {
        // render view
        return view('livewire.upload');
    }
}
