<div>
    {{-- The whole world belongs to you. --}}
    <div class="card mt-4">
        <div class="card-body">
            <div class="mb-3">
                <form wire:submit.prevent="save">
                    @error('transaction')
                        <div class="alert alert-danger" role="alert">
                            {{ $message }}
                        </div>
                    @enderror
                    <div class="input-group mb-3">
                        <input class="form-control" type="file" wire:model="transaction">
                        <div class="input-group-append">
                            <button class="btn btn-outline-secondary" type="submit">Save</button>
                        </div>
                    </div>
                    <div wire:loading wire:target="save">Uploading...</div>
                </form>
            </div>
        </div>
    </div>
</div>
