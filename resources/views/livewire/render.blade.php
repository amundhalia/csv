<div>
    {{-- If your happiness depends on money, you will never be happy with yourself. --}}
    @if(!blank($records))
        <div class="card mt-3">
            <div class="card-body">
                <table class="table table-striped">
                    <thead>
                        <tr>
                          <th scope="col">Date</th>
                          <th scope="col">Transaction Number</th>
                          <th scope="col">Customer Number</th>
                          <th scope="col">Reference</th>
                          <th scope="col">Amount</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($records as $record)
                                <th>{{ \Carbon\Carbon::parse($record['Date'])->format('d-m-Y H:m') }}</th>
                                <td>
                                    {{ $record['TransactionNumber'] }}
                                    @if(!$record['status'])
                                        <span class="text-danger">(invalid)</span>
                                    @endif
                                </td>
                                <td>{{ $record['CustomerNumber'] }}</td>
                                <td>{{ $record['Reference'] }}</td>
                                <td>
                                    @if($record['Amount'] > 0)
                                        <span class="text-primary">${{ number_format($record['Amount']/100, 2) }}<span>
                                    @else
                                        <span class="text-danger">$-{{ abs(number_format($record['Amount']/100, 2)) }}<span>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    @endif
</div>
