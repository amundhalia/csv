@extends('layouts.app')

@section('content')

    @livewire('upload')
    @livewire('render')
@endsection
