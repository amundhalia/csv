# Coding Challange

# Framwork
Laravel 8 has been used for this project.

## Packages
- Livewire
- league

# Docker
For docker I have used docker composed with laradock. To install follow below:
```bash
# clone laradock repo
git clone https://github.com/Laradock/laradock.git

# navigate into the directory
cd laradock

#copy .env file
cp .env.example .env

# modyfy nginx default file 
nano nginx/sites/default.conf

# start containers (try sudo if fails)
docker-compose up -d redis nginx workspace php-fpm

# now navigate into the container (try sudo if fails)
docker-compose exec --user=laradock workspace bash

```
 
## Install
To install laravel please follow below steps:
```bash
# once inside the container clone repo
git clone git@bitbucket.org:amundhalia/csv.git

# install composer packages
composer install

# install npm
npm install

# create .env file
cp .env.example .env

# setup redis connection in .env
REDIS_HOST=redis
REDIS_PASSWORD=null
REDIS_PORT=6379
```

# Aproach
I have used livewire for this project, livewire is an amazing package allows me to write all my program in php while using javascript at the same time.
